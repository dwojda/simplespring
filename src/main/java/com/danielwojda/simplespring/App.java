package com.danielwojda.simplespring;

import com.danielwojda.simplespring.config.AppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {

        ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);

        Service service =  appContext.getBean("service", Service.class);

        System.out.println(service.getSnippets());
    }
}
