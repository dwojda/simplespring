package com.danielwojda.simplespring;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class SnippetRepositoryImpl implements SnippetRepository {

    private String prefix;


    @Autowired
    public SnippetRepositoryImpl(@Value("${prefix}") String prefix) {
        this.prefix = prefix;
    }


    @Override
    public List<String> findAll() {
        return Arrays.asList(prefix+"-snippet1", prefix+"-snippet2");
    }

}
