package com.danielwojda.simplespring;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class Service {

    private SnippetRepository repository;


    @Autowired
    public Service(SnippetRepository repository) {
        this.repository = repository;
    }


    public List<String> getSnippets() {
        return repository.findAll();
    }

}
