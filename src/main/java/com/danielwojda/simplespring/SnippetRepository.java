package com.danielwojda.simplespring;

import java.util.List;

public interface SnippetRepository {
    List<String> findAll();
}
